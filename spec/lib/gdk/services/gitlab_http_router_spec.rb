# frozen_string_literal: true

describe GDK::Services::GitLabHttpRouter do # rubocop:disable RSpec/FilePath
  describe '#name' do
    it { expect(subject.name).to eq('gitlab-http-router') }
  end

  describe '#command' do
    it 'returns the necessary command to run gitlab-http-router' do
      expect(subject.command).to eq('support/exec-cd gitlab-http-router npx wrangler dev -e dev --port 3000 --var GITLAB_PROXY_HOST:127.0.0.1:3000')
    end

    context 'when enabled' do
      before do
        config = {
          'gitlab_http_router' => {
            'enabled' => true
          }
        }

        stub_gdk_yaml(config)
      end

      it "returns the command with the workhorse address" do
        expect(subject.command).to eq('support/exec-cd gitlab-http-router npx wrangler dev -e dev --port 3000 --var GITLAB_PROXY_HOST:127.0.0.1:3333')
      end

      context 'when nginx is enabled' do
        before do
          config = {
            'gitlab_http_router' => {
              'enabled' => true
            },
            'nginx' => {
              'enabled' => true
            }
          }

          stub_gdk_yaml(config)
        end

        it "returns the command with the nginx address" do
          expect(subject.command).to eq('support/exec-cd gitlab-http-router npx wrangler dev -e dev --port 3000 --var GITLAB_PROXY_HOST:127.0.0.1:8080')
        end
      end

      context 'when use_distinct_port' do
        before do
          config = {
            'gitlab_http_router' => {
              'enabled' => true,
              'use_distinct_port' => true
            }
          }

          stub_gdk_yaml(config)
        end

        it "returns the command using the distinct port" do
          expect(subject.command).to eq('support/exec-cd gitlab-http-router npx wrangler dev -e dev --port 9393 --var GITLAB_PROXY_HOST:127.0.0.1:3333')
        end
      end
    end
  end

  describe '#ready_message' do
    it 'returns the default ready message' do
      expect(subject.ready_message).to eq('The HTTP Router is available at http://127.0.0.1:3000.')
    end
  end

  describe '#enabled?' do
    it 'is disabled by default' do
      expect(subject.enabled?).to be(false)
    end
  end
end
