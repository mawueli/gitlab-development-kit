# frozen_string_literal: true

module GDK
  module Services
    class GitLabHttpRouter < Base
      def name
        'gitlab-http-router'
      end

      def command
        "support/exec-cd gitlab-http-router npx wrangler dev -e dev --port #{active_port} --var GITLAB_PROXY_HOST:#{gitlab_host}"
      end

      def ready_message
        "The HTTP Router is available at #{listen_address}."
      end

      def enabled?
        config.gitlab_http_router.enabled?
      end

      private

      def listen_address
        URI::HTTP.build(host: config.hostname, port: active_port)
      end

      def active_port
        if config.gitlab_http_router.use_distinct_port?
          config.gitlab_http_router.port
        else
          config.port
        end
      end

      def gitlab_host
        if config.nginx?
          config.nginx.__listen_address
        else
          config.workhorse.__listen_address
        end
      end
    end
  end
end
